# Jobzer

Agrégateur d'annonces d'offres d'emploi visant à faciliter la recherche et la candidature à ces dernières. Application réalisée dans le cadre du projet I1 IPST-CNAM 2019-2020.

Equipe composée de:
  - Vincent Galy
  - Rémy Pourrech
  - Mathias Foix
  - Théodore Marestin

## Programmes requis
 
 - [GIT](https://git-scm.com/downloads)
 - [NodeJS](https://nodejs.org/en/download/)

## I. Mise en place de l'environnement

1. Cloner le repository

        git clone https://gitlab.com/jobzer/jobzer-frontend.git
        
2. Installer les paquets
        
        npm install
  
## II. Installation et utilisation de JSON-Server (Mock API)

**Note Importante**: Lors des tests, il se peut que ces derniers "FAIL" à cause du json-server. En effet, dû à un problème mémoire, ce dernier peut planter lors de requêtes trop nombreuses.

**Note Importante #2**: L'URL de l'API peut être modifiée dans les fichiers services se trouvant dans ce dossier : *src/app/shared/services*

> Le premier lien est le chemin vers l'API en production, le second, en local, lors de tests.

1. Installer JSON-Server

        npm install -g json-server
        
2. Démarrer le serveur
        
        json-server -q -w mockdb.json
        
L'API Rest est maintenant accessible à l'URL: http://localhost:3000.
Cela fonctionne comme avec une API REST basique (GET, POST, PUT...).
Voir plus d'informations [ici](https://www.npmjs.com/package/json-server).

_Note_: le fichier mockdb.json sert de base pour créer les routes ainsi que de moyen de stockage d'informations.

## Quelques commandes utiles

1. Lancer l'application en [local](http://localhost:4200)

        ng server -o
        
2. Build l'application pour un déploiement

        ng build --prod
        
3. Lancer les tests unitaires

        ng test --watch=false
  
4. Lancer les tests d'interface

        ng e2e --port 5000
    
5. Vérifier que le code respecte la norme de codage ESLint

        ng lint

### Informations de connexion

Afin d'utiliser l'application, vous pouvez créer un compte ou utiliser celui que l'on a créé.

    Email: joe@moma.com
    Mot de passe: plain-text-password
