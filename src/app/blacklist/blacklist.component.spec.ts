import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BlacklistComponent} from './blacklist.component';
import {AuthenticationService} from '../shared/services/authentication.service';
import {JobsService} from '../shared/services/jobs.service';
import {FaIconComponent, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {TranslateModule} from '@ngx-translate/core';
import {User} from '../shared/models/user.model';
import {Job} from '../shared/models/job.model';

describe('BlacklistComponent', () => {
  let component: BlacklistComponent;
  let fixture: ComponentFixture<BlacklistComponent>;
  let authenticationService: AuthenticationService;
  let jobsService: JobsService;
  const testUser: User = {
    id: 0,
    email: 'test@user.com',
    password: 'mypassword',
    emailVerified: true,
    profilePicture: '',
    informations: {
      firstname: 'User',
      lastname: 'Test',
      birthdate: '1975-08-13'
    },
    likedJobs: [],
    dislikedJobs: [],
    skills: [
      {
        id: 1,
        name: 'C#',
        weight: 1
      }
    ]
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [BrowserModule, FontAwesomeModule, ReactiveFormsModule, FormsModule, HttpClientModule, TranslateModule.forRoot()],
      declarations: [ BlacklistComponent, FaIconComponent ],
      providers: [ AuthenticationService, JobsService ]
    })
    .compileComponents();

    // Mock localStorage
    let store: object = {};
    spyOn(window.localStorage, 'getItem').and.callFake((key: string) => {
      return key in store ? JSON.parse(((store as any)[key] as string) || '{}') : '{}';
    });
    spyOn(window.localStorage, 'setItem').and.callFake((key: string, value: string) => {
      return (store as any)[key] = JSON.stringify(value);
    });
    spyOn(window.localStorage, 'clear').and.callFake(() => {
      store = {};
    });
    spyOn(window.localStorage, 'removeItem').and.callFake((key: string) => {
      delete (store as any)[key];
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlacklistComponent);
    component = fixture.componentInstance;
    authenticationService = TestBed.inject(AuthenticationService);
    jobsService = TestBed.inject(JobsService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('job service should create', () => {
    expect(jobsService).toBeTruthy();
  });

  it('authentification service should create', () => {
    expect(authenticationService).toBeTruthy();
  });

  it('should remove a job to user\'s blacklist', () => {
    const jobToAdd: Job = {
      id: 3,
      company: 'La Rochelle Rugby',
      email: 'rochelle@test.fr',
      phone: '+9985959959',
      title: 'Dev web',
      detail: 'Pour faire un site',
      skills: [
        {
          id: 3,
          name: 'HTML',
          weight: 1
        },
        {
          id: 2,
          name: 'JavaScript',
          weight: 1
        }
      ]
    };
    if (testUser && testUser.dislikedJobs) {
      testUser.dislikedJobs.push(jobToAdd.id);
      testUser.dislikedJobs.splice(0, 1);
      expect(testUser.dislikedJobs.length === 0).toBeTruthy();
    }
  });
});
