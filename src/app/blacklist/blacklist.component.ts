import {Component, OnDestroy, OnInit} from '@angular/core';
import {Job} from '../shared/models/job.model';
import {User} from '../shared/models/user.model';
import {JobsService} from '../shared/services/jobs.service';
import {AuthenticationService} from '../shared/services/authentication.service';
import {forkJoin, Observable, Subscription} from 'rxjs';

@Component({
  selector: 'app-blacklist',
  templateUrl: './blacklist.component.html',
  styleUrls: ['./blacklist.component.scss']
})
export class BlacklistComponent implements OnInit, OnDestroy {

  public aBlacklistedJob: Job[] = [];
  private localUser: User = new User();

  // Subscriptions
  private aSubscriptions: Subscription[] = [];

  constructor(private jobsService: JobsService, private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    // Get Logged User
    this.localUser = JSON.parse(localStorage.getItem('currentUser') || '{}');

    // Batch request to get details on Liked Jobs
    if (this.localUser && this.localUser.dislikedJobs && this.localUser.dislikedJobs.length > 0) {
      const aBatchRequest: Observable<Job>[] = [];
      this.localUser.dislikedJobs.forEach((skillId: number) => {
        aBatchRequest.push(this.jobsService.getJobById(skillId));
      });
      if (aBatchRequest.length > 0) {
        forkJoin(aBatchRequest).subscribe((jobs: Job[]) => {
          jobs.forEach((job: Job) => this.aBlacklistedJob.push(job));
        });
      }
    }
  }

  ngOnDestroy(): void {
    this.aSubscriptions.forEach((subscription: Subscription) => subscription.unsubscribe());
  }

  /**
   * Add the job to the user's favorite and removes it from the blacklist
   * @param jobId: number
   */
  addToFavorite(jobId: number): void {
    if (this.localUser && this.localUser.likedJobs) {
      this.localUser.likedJobs.push(jobId);
      const updateAccountSubscription = this.authenticationService.updateAccount(this.localUser).subscribe((user: User) => {
        this.localUser = user;
        localStorage.setItem('currentUser', JSON.stringify(this.localUser));
        this.deleteFromBlacklist(jobId);
      });
      this.aSubscriptions.push(updateAccountSubscription);
    }
  }

  /**
   * Delete a job from user's blacklist
   * @param jobToDeleteId: number
   */
  deleteFromBlacklist(jobToDeleteId: number): void {
    if (this.localUser && this.localUser.dislikedJobs && this.localUser.dislikedJobs.length > 0) {
      // Delete the blacklisted job from the User
      this.localUser.dislikedJobs.forEach((jobId: number, index: number) => {
        if (this.localUser.dislikedJobs && jobId === jobToDeleteId) {
          this.localUser.dislikedJobs.splice(index, 1);
        }
      });
      const updateAccountSubscription = this.authenticationService.updateAccount(this.localUser).subscribe((user: User) => {
        this.localUser = user;
        localStorage.setItem('currentUser', JSON.stringify(this.localUser));

        // Update List
        this.aBlacklistedJob.forEach((job: Job, index: number) => {
          if (job.id === jobToDeleteId) {
            this.aBlacklistedJob.splice(index, 1);
          }
        });
      });
      this.aSubscriptions.push(updateAccountSubscription);
    }
  }

  /**
   * Track object's ID so it doesn't reload the whole DOM
   * @param index: number
   * @param item: Skill
   * @return item's id or null
   */
  trackByFn(index: number, item: Job): number|null {
    if (!item) return null;
    return item.id;
  }

}
