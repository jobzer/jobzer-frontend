import {Component, OnDestroy, OnInit} from '@angular/core';
import {JobsService} from '../shared/services/jobs.service';
import {Job} from '../shared/models/job.model';
import {User} from '../shared/models/user.model';
import {AuthenticationService} from '../shared/services/authentication.service';
import {Subscription} from 'rxjs';
import {Skill} from '../shared/models/skill.model';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss']
})
export class JobsComponent implements OnInit, OnDestroy {

  public aJobs: Job[] = [];
  public jobToDisplay: Job = new Job();
  public iCurrentIndex: number = 0;
  public localUser: User = new User();

  // Subscriptions
  private aSubscriptions: Subscription[] = [];

  constructor(private jobsService: JobsService, private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    const getAllJobsSubscription = this.jobsService.getAllJobs().subscribe((jobs: Job[]) => {
      if (jobs.length > 0) {
        // We only add jobs which aren't blacklisted or already favorited and if user is qualified for it
        jobs.forEach((job: Job) => {
          if (!this.isJobRefusedOrFavorited(job.id) && this.isUserQualifiedEnough(job)) {
            this.aJobs.push(job);
          }
        });
        if (this.aJobs.length > 0) {
          this.jobToDisplay = this.aJobs[0];
          this.iCurrentIndex = 0;
        }
      }
    });
    this.aSubscriptions.push(getAllJobsSubscription);
    this.localUser = JSON.parse(localStorage.getItem('currentUser') || '{}');
  }

  ngOnDestroy(): void {
    this.aSubscriptions.forEach((subscription: Subscription) => subscription.unsubscribe());
  }

  /**
   * Check if user have the skill
   * @param skillId: number
   * @returns true | false
   */
  hasSkill(skillId: number): boolean {
    return (this.localUser && this.localUser.skills) ? !!this.localUser.skills.find(skill => skill.id === skillId) : false;
  }

  /**
   * Check if the job to display is already refused or favorited by the user
   * @param jobId: number
   * @returns true | false
   */
  isJobRefusedOrFavorited(jobId: number): boolean {
    return (this.localUser && this.localUser.dislikedJobs && this.localUser.likedJobs) ? (this.localUser.dislikedJobs.includes(jobId) || this.localUser.likedJobs.includes(jobId)) : false;
  }

  /**
   * Check if the user is qualified enough for a job depending on common skill number & skill weight comparison
   * Skill's weight are deducted from the CIGREF (need reading to extract real weight)
   * @param job: Job
   * @returns true | false
   */
  isUserQualifiedEnough(job: Job): boolean {
    let minimumSkillNumber = false;
    let minimumSkillWeight = false;

    let commonSkillsNumber: number = 0;

    let totalJobSkillWeight: number = 0;
    let totalUserSkillWeight: number = 0;

    let averageSkillJob: number = 0;
    let averageSkillUser: number = 0;
    let minimumSkillWeightUser: number = Number.POSITIVE_INFINITY;

    if (job && job.skills && job.skills.length > 0) {
      job.skills.forEach((skill: Skill) => {
        totalJobSkillWeight += skill.weight;
      });
      averageSkillJob = totalJobSkillWeight / job.skills.length;

      if (this.localUser && this.localUser.skills && this.localUser.skills.length > 0) {
        this.localUser.skills.forEach((skill: Skill) => {
          totalUserSkillWeight += skill.weight;

          // Get the the skill the least weight
          if (skill.weight < minimumSkillWeightUser) {
            minimumSkillWeightUser = skill.weight;
          }
        });

        commonSkillsNumber = job.skills.filter((jobSkill: Skill) => this.localUser && this.localUser.skills && this.localUser.skills.find((userSkill: Skill) => userSkill.id === jobSkill.id)).length;
        averageSkillUser = totalUserSkillWeight / this.localUser.skills.length;
      }

      // Check if user have at least 1/4 of the skills required
      if (commonSkillsNumber >= (job.skills.length / 4)) {
        minimumSkillNumber = true;
      }

      // Check if user have at least the minimum total skill's weight amount required
      if (averageSkillUser >= (averageSkillJob - minimumSkillWeightUser)) {
        minimumSkillWeight = true;
      }
    }

    /* console.log(
        'Job', job,
        '\ncommonSkillsNumber', commonSkillsNumber,
        '\nminimumSkillNumber', minimumSkillNumber,
        '\nminimumSkillWeight', minimumSkillWeight,
        '\naverageSkillUser',averageSkillUser,
        '\naverageSkillJob',averageSkillJob,
        '\nminimumSkillWeightUser',minimumSkillWeightUser,
        '\ntotalUserSkillWeight',totalUserSkillWeight,
        '\ntotalJobSkillWeight',totalJobSkillWeight,
        '\nisQualified?', minimumSkillNumber && minimumSkillWeight
      );
      */

    return minimumSkillNumber && minimumSkillWeight;
  }

  /**
   * Add a job to user's favorite
   * @param jobId: number
   */
  addToFavorite(jobId: number): void {
    if (this.localUser && this.localUser.likedJobs) {
      this.localUser.likedJobs.push(jobId);
      const updateAccountSubscription = this.authenticationService.updateAccount(this.localUser).subscribe((user: User) => {
        this.localUser = user;
        localStorage.setItem('currentUser', JSON.stringify(this.localUser));
        this.aJobs.forEach((job: Job, index: number) => {
          if (job.id === jobId) {
            this.aJobs.splice(index, 1);
          }
        });
        this.showNextJob();
      });
      this.aSubscriptions.push(updateAccountSubscription);
    }
  }

  /**
   * Add a job to user's blacklist
   * @param jobId: number
   */
  addToBlacklist(jobId: number): void {
    if (this.localUser && this.localUser.dislikedJobs) {
      this.localUser.dislikedJobs.push(jobId);
      const updateAccountSubscription = this.authenticationService.updateAccount(this.localUser).subscribe((user: User) => {
        this.localUser = user;
        localStorage.setItem('currentUser', JSON.stringify(this.localUser));
        this.aJobs.forEach((job: Job, index: number) => {
          if (job.id === jobId) {
            this.aJobs.splice(index, 1);
          }
        });
        this.showNextJob();
      });
      this.aSubscriptions.push(updateAccountSubscription);
    }
  }

  /**
   * Display the next job in the array
   */
  showNextJob(): void {
    if (this.aJobs[this.iCurrentIndex + 1]) {
      this.jobToDisplay = this.aJobs[this.iCurrentIndex + 1];
      this.iCurrentIndex++;
    } else if (!this.aJobs[this.iCurrentIndex + 1] && this.aJobs[0]) {
      this.jobToDisplay = this.aJobs[0];
      this.iCurrentIndex = 0;
    } else {
      this.jobToDisplay = new Job();
    }
  }

  /**
   * Track object's ID so it doesn't reload the whole DOM
   * @param index: number
   * @param item: Skill
   * @return item's id or null
   */
  trackByFn(index: number, item: Skill): number|null {
    if (!item) return null;
    return item.id;
  }

}
