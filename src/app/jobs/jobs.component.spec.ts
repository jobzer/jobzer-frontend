import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {JobsComponent} from './jobs.component';
import {JobsService} from '../shared/services/jobs.service';
import {BrowserModule} from '@angular/platform-browser';
import {FaIconComponent, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthenticationService} from '../shared/services/authentication.service';
import {HttpClientModule} from '@angular/common/http';
import {TranslateModule} from '@ngx-translate/core';
import {User} from '../shared/models/user.model';
import {Job} from '../shared/models/job.model';

describe('JobsComponent', () => {
  let component: JobsComponent;
  let fixture: ComponentFixture<JobsComponent>;
  let jobService: JobsService;
  let authenticationService: AuthenticationService;
  const testUser: User = {
    id: 0,
    email: 'test@user.com',
    password: 'mypassword',
    emailVerified: true,
    profilePicture: '',
    informations: {
      firstname: 'User',
      lastname: 'Test',
      birthdate: '1975-08-13'
    },
    likedJobs: [],
    dislikedJobs: [],
    skills: [
      {
        id: 1,
        name: 'C#',
        weight: 1
      }
    ]
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [BrowserModule, FontAwesomeModule, ReactiveFormsModule, FormsModule, HttpClientModule, TranslateModule.forRoot()],
      declarations: [ JobsComponent, FaIconComponent ],
      providers: [JobsService, AuthenticationService]
    })
    .compileComponents();

    // Mock localStorage
    let store: object = {};
    spyOn(window.localStorage, 'getItem').and.callFake((key: string) => {
      return key in store ? JSON.parse(((store as any)[key] as string) || '{}') : '{}';
    });
    spyOn(window.localStorage, 'setItem').and.callFake((key: string, value: string) => {
      return (store as any)[key] = JSON.stringify(value);
    });
    spyOn(window.localStorage, 'clear').and.callFake(() => {
      store = {};
    });
    spyOn(window.localStorage, 'removeItem').and.callFake((key: string) => {
      delete (store as any)[key];
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobsComponent);
    component = fixture.componentInstance;
    jobService = TestBed.inject(JobsService);
    authenticationService = TestBed.inject(AuthenticationService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('job service should create', () => {
    expect(jobService).toBeTruthy();
  });

  it('authentification service should create', () => {
    expect(authenticationService).toBeTruthy();
  });

  it('should add a job to user\'s favorite', () => {
    const jobToAdd: Job = {
      id: 3,
      company: 'La Rochelle Rugby',
      email: 'rochelle@test.fr',
      phone: '+9985959959',
      title: 'Dev web',
      detail: 'Pour faire un site',
      skills: [
        {
          id: 3,
          name: 'HTML',
          weight: 1
        },
        {
          id: 2,
          name: 'JavaScript',
          weight: 1
        }
      ]
    };
    if (testUser && testUser.likedJobs) {
      testUser.likedJobs.push(jobToAdd.id);
      expect(testUser.likedJobs.length > 0 && testUser.likedJobs[0] === 3).toBeTruthy();
    }
  });

  it('should add a job to user\'s blacklist', () => {
    const jobToAdd: Job = {
      id: 3,
      company: 'La Rochelle Rugby',
      email: 'rochelle@test.fr',
      phone: '+9985959959',
      title: 'Dev web',
      detail: 'Pour faire un site',
      skills: [
        {
          id: 3,
          name: 'HTML',
          weight: 1
        },
        {
          id: 2,
          name: 'JavaScript',
          weight: 1
        }
      ]
    };
    if (testUser && testUser.dislikedJobs) {
      testUser.dislikedJobs.push(jobToAdd.id);
      expect(testUser.dislikedJobs.length > 0 && testUser.dislikedJobs[0] === 3).toBeTruthy();
    }
  });
});
