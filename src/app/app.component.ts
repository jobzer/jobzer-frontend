import {Component} from '@angular/core';
import {AuthenticationService} from './shared/services/authentication.service';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'jobzer-frontend';
  langSelect: string;

  constructor(public authenticationService: AuthenticationService, public router: Router, public translate: TranslateService) {
    translate.addLangs(['en', 'fr']);
    translate.setDefaultLang('en');

    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
    this.langSelect = translate.currentLang;
  }

  logout(): void {
    this.authenticationService.logout();
    this.router.navigate(['login']).then();
  }
}
