import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import {NoPageFoundComponent} from './no-page-found/no-page-found.component';
import {RegisterComponent} from './register/register.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthenticationService} from './shared/services/authentication.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {ProfileComponent} from './profile/profile.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {JobsComponent} from './jobs/jobs.component';
import {JobsService} from './shared/services/jobs.service';
import {SkillsService} from './shared/services/skills.service';
import {FavoriteComponent} from './favorite/favorite.component';
import {BlacklistComponent} from './blacklist/blacklist.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {DeleteAccountComponent} from './shared/modals/deleteAccountModal/deleteAccount.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    NoPageFoundComponent,
    RegisterComponent,
    ProfileComponent,
    JobsComponent,
    FavoriteComponent,
    BlacklistComponent,
    DeleteAccountComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FontAwesomeModule,
    FormsModule,
    HttpClientModule,
    NgSelectModule,
    ReactiveFormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    BrowserAnimationsModule,
    MatDialogModule,
    MatButtonModule,
    MatDividerModule
  ],
  providers: [AuthenticationService, JobsService, SkillsService],
  entryComponents: [DeleteAccountComponent],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
