import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {NoPageFoundComponent} from './no-page-found/no-page-found.component';
import {HomeComponent} from './home/home.component';
import {RegisterComponent} from './register/register.component';
import {ProfileComponent} from './profile/profile.component';
import {AuthGuard} from './shared/guards/AuthGuard';
import {JobsComponent} from './jobs/jobs.component';
import {FavoriteComponent} from './favorite/favorite.component';
import {BlacklistComponent} from './blacklist/blacklist.component';


const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
  {path: 'jobs', component: JobsComponent, canActivate: [AuthGuard]},
  {path: 'jobs/favorite', component: FavoriteComponent, canActivate: [AuthGuard]},
  {path: 'jobs/blacklist', component: BlacklistComponent, canActivate: [AuthGuard]},
  {path: '**', component: NoPageFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule {
}
