import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthenticationService} from '../shared/services/authentication.service';
import {User} from '../shared/models/user.model';
import {Skill} from '../shared/models/skill.model';
import {SkillsService} from '../shared/services/skills.service';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {MatDialog} from '@angular/material/dialog';
import {DeleteAccountComponent} from '../shared/modals/deleteAccountModal/deleteAccount.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {

  public loggedUser: User = new User();
  public passwordVerified: string = '';
  public isProfileUpdated: boolean = false;
  public userSkills: string[] = [];
  public allSkills: Skill[] = [];
  public hasUploadError = {error: false, message: ''};
  private aSubscriptions: Subscription[] = [];

  constructor(private authenticationService: AuthenticationService, private skillsService: SkillsService, private router: Router, private translate: TranslateService, public dialog: MatDialog) { }

  ngOnInit(): void {
    // Get All Skills
    const getAllSkillsSubscriptions = this.skillsService.getAllSkills().subscribe((skills: Skill[]) => this.allSkills = skills);
    this.aSubscriptions.push(getAllSkillsSubscriptions);
    // Get Local User
    this.loggedUser = JSON.parse(localStorage.getItem('currentUser') || '{}');
    if (this.loggedUser && this.loggedUser.skills && this.loggedUser.skills.length > 0) {
      this.userSkills = this.loggedUser.skills.map((skill: Skill) => skill.name);
    }
  }

  ngOnDestroy(): void {
    this.aSubscriptions.forEach((subscription: Subscription) => subscription.unsubscribe());
  }

  /**
   * Try to convert the profil picture to base64
   * @summary imageValideType & image size < 5MB
   * @param event: Object
   */
  public handleProfilePicture(event: object): void {
    if (this.loggedUser) {
      const imageValidType = ['image/png', 'image/jpeg', 'image/jpg'];
      // @ts-ignore: 'target' exists in this case (input type file)
      const imageToUpload = event.target.files[0];

      if (!imageValidType.includes(imageToUpload.type)) {
        this.hasUploadError = {error: true, message: this.translate.instant('utils.fileUpload.images.format', {format: ' .png, .jpeg, .jpg'})};
        return;
      } else if (imageToUpload.size > 5000000) {
        this.hasUploadError = {error: true, message: this.translate.instant('utils.fileUpload.fileSize', {maxSize: '5mb'})};
        return;
      }

      const reader = new FileReader();
      reader.readAsBinaryString(imageToUpload);
      reader.onload = () => {
        if (typeof reader.result === 'string') {
          this.loggedUser.profilePicture = `data:${imageToUpload.type};base64,` + btoa(reader.result);
          this.hasUploadError = {error: false, message: ''};
        }
      };
      reader.onerror = (error) => {
        this.hasUploadError = {error: true, message: this.translate.instant('utils.fileUpload.error', {error: (error.target ? error.target.result : 'Unknown Error')})};
      };
    }
  }

  /**
   * Try to update user's profile data
   */
  public updateProfile(): void {
    if (this.loggedUser) {
      this.hasUploadError = {error: false, message: ''};
      if (this.loggedUser.password !== this.passwordVerified) {
        this.hasUploadError = {error: true, message: this.translate.instant('register.confirmPasswordMustMatch')};
        return;
      }
      const updateAccountSubscription = this.authenticationService.updateAccount(this.loggedUser).subscribe((user: User) => {
        this.loggedUser = user;
        localStorage.setItem('currentUser', JSON.stringify(this.loggedUser));
        this.isProfileUpdated = true;
      });
      this.aSubscriptions.push(updateAccountSubscription);
    }
  }

  /**
   * Try to update user's skills
   * @param event: number[]
   */
  public onChangeUserSkill(event: number[]): void {
    if (this.loggedUser && this.loggedUser.skills) {
      if (event.length > 0) {
        // Delete skills removed by the user
        this.loggedUser.skills.forEach((skill: Skill, index: number) => {
          if (this.loggedUser.skills && !event.includes(skill.id)) {
            this.loggedUser.skills.splice(index, 1);
          }
        });

        // Add new skills
        event.forEach((skillId: number) => {
          if (this.loggedUser.skills && !this.loggedUser.skills.find((skill: Skill) => skill.id === skillId)) {
            const skillToAdd = this.allSkills.find((skill: Skill) => skill.id === skillId);
            if (skillToAdd) {
              this.loggedUser.skills.push(skillToAdd);
            }
          }
        });
      } else {
        this.loggedUser.skills = [];
      }
    }
  }

  /**
   * Try to delete user's account
   */
  deleteMyAccount(): void {
    if (this.loggedUser) {
      const dialogRef = this.dialog.open(DeleteAccountComponent, {
        width: '1000px',
        height: '200px'
      });

      const dialog = dialogRef.afterClosed().subscribe(result => {
        if (result === 'deleteAccount') {
          const deleteAccountSubscription = this.authenticationService.deleteAccount(this.loggedUser).subscribe((register: User) => {
            if (Object.keys(register).length === 0) {
              this.authenticationService.logout();
              this.router.navigate(['home']).then();
            }
          });
          this.aSubscriptions.push(deleteAccountSubscription);
        }
      });
      this.aSubscriptions.push(dialog);
    }
  }

  /**
   * Track object's ID so it doesn't reload the whole DOM
   * @param index: number
   * @param item: Skill
   * @return item's id or null
   */
  trackByFn(index: number, item: Skill): number|null {
    if (!item) return null;
    return item.id;
  }

}
