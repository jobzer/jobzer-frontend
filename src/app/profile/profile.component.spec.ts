import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ProfileComponent} from './profile.component';
import {BrowserModule} from '@angular/platform-browser';
import {FaIconComponent, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthenticationService} from '../shared/services/authentication.service';
import {SkillsService} from '../shared/services/skills.service';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {TranslateModule} from '@ngx-translate/core';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MatDialogModule} from '@angular/material/dialog';
import {User} from '../shared/models/user.model';
import {Skill} from '../shared/models/skill.model';

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;
  let authenticationService: AuthenticationService;
  let skillsService: SkillsService;
  const testUser: User = {
    id: 0,
    email: 'test@user.com',
    password: 'mypassword',
    emailVerified: true,
    profilePicture: '',
    informations: {
      firstname: 'User',
      lastname: 'Test',
      birthdate: '1975-08-13'
    },
    likedJobs: [],
    dislikedJobs: [],
    skills: [
      {
        id: 1,
        name: 'C#',
        weight: 1
      }
    ]
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [BrowserModule, FontAwesomeModule, ReactiveFormsModule, FormsModule, HttpClientModule, RouterTestingModule, TranslateModule.forRoot(), MatDialogModule],
      declarations: [ProfileComponent, FaIconComponent],
      providers: [AuthenticationService, SkillsService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();

    // Mock localStorage
    let store: object = {};
    spyOn(window.localStorage, 'getItem').and.callFake((key: string) => {
      return key in store ? JSON.parse(((store as any)[key] as string) || '{}') : '{}';
    });
    spyOn(window.localStorage, 'setItem').and.callFake((key: string, value: string) => {
      return (store as any)[key] = JSON.stringify(value);
    });
    spyOn(window.localStorage, 'clear').and.callFake(() => {
      store = {};
    });
    spyOn(window.localStorage, 'removeItem').and.callFake((key: string) => {
      delete (store as any)[key];
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    authenticationService = TestBed.inject(AuthenticationService);
    skillsService = TestBed.inject(SkillsService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('authentication service should create', () => {
    expect(authenticationService).toBeTruthy();
  });

  it('skill service should create', () => {
    expect(skillsService).toBeTruthy();
  });

  it('should add a skill to the user', () => {
    const skillToAdd: Skill = {
      id: 5,
      name: '.NET',
      weight: 1
    };
    if (testUser && testUser.skills) {
      testUser.skills.push(skillToAdd);
      expect(testUser.skills.length === 2 &&
        testUser.skills[0].id === 1 &&
        testUser.skills[1].id === 5).toBeTruthy();
    }
  });

  it('should remove a skill to the user', () => {
    if (testUser && testUser.skills) {
      testUser.skills.splice(1, 1);
      expect(testUser.skills.length === 1 &&
        testUser.skills[0].id === 1).toBeTruthy();
    }
  });

  it('should change user\'s email', () => {
    if (testUser) {
      testUser.email = 'newmail@newmail.fr';
      expect(testUser.email === 'newmail@newmail.fr').toBeTruthy();
    }
  });

  it('should change user\'s password', () => {
    if (testUser) {
      testUser.password = 'anewpasswordtest';
      expect(testUser.password === 'anewpasswordtest').toBeTruthy();
    }
  });

  it('should change user\'s profile picture', () => {
    if (testUser) {
      testUser.profilePicture = 'encodedbase64image';
      expect(testUser.profilePicture === 'encodedbase64image').toBeTruthy();
    }
  });

  it('should change user\'s informations', () => {
    if (testUser && testUser.informations) {
      testUser.informations.firstname = 'newfirstname';
      testUser.informations.lastname = 'newlastname';
      testUser.informations.birthdate = '2003-10-24';
      expect(testUser.informations.firstname === 'newfirstname' &&
      testUser.informations.lastname === 'newlastname' &&
      testUser.informations.birthdate === '2003-10-24').toBeTruthy();
    }
  });
});
