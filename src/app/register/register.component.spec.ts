import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {RegisterComponent} from './register.component';
import {BrowserModule} from '@angular/platform-browser';
import {FaIconComponent, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthenticationService} from '../shared/services/authentication.service';
import {User} from '../shared/models/user.model';
import {HttpClientModule} from '@angular/common/http';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let authenticationService: AuthenticationService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [BrowserModule, FontAwesomeModule, ReactiveFormsModule, FormsModule, HttpClientModule, RouterTestingModule, TranslateModule.forRoot()],
      declarations: [RegisterComponent, FaIconComponent],
      providers: [AuthenticationService]
    })
      .compileComponents();

    // Mock localStorage
    let store: object = {};
    spyOn(window.localStorage, 'getItem').and.callFake((key: string) => {
      return key in store ? JSON.parse(((store as any)[key] as string) || '{}') : '{}';
    });
    spyOn(window.localStorage, 'setItem').and.callFake((key: string, value: string) => {
      return (store as any)[key] = JSON.stringify(value);
    });
    spyOn(window.localStorage, 'clear').and.callFake(() => {
      store = {};
    });
    spyOn(window.localStorage, 'removeItem').and.callFake((key: string) => {
      delete (store as any)[key];
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    authenticationService = TestBed.inject(AuthenticationService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('authentication service should create', () => {
    expect(authenticationService).toBeTruthy();
  });

  it('Should pass register and deletion', (done) => {
    const user: User = {
      email: 'bob@test.fr',
      password: 'password',
      profilePicture: '',
      emailVerified: true,
      informations: {firstname: 'bob', lastname: 'test', birthdate: '18-04-1997'}
    };

    authenticationService.register(user).subscribe((register: User) => {
      if (register && register.id) {
        authenticationService.deleteAccount(register).subscribe((deletion: User) => {
          expect(deletion && Object.keys(deletion).length === 0).toBeTruthy();
          done();
        });
      } else {
        fail('User could not be registered');
        done();
      }
    });
  });

});
