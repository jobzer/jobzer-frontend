import {Component, OnInit} from '@angular/core';
import {faExclamation} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-no-page-found',
  templateUrl: './no-page-found.component.html',
  styleUrls: ['./no-page-found.component.scss']
})
export class NoPageFoundComponent implements OnInit {

  // FontAwesome
  faExclamation = faExclamation;

  constructor() {
  }

  ngOnInit(): void {
  }

}
