import {Component, OnDestroy, OnInit} from '@angular/core';
import {JobsService} from '../shared/services/jobs.service';
import {AuthenticationService} from '../shared/services/authentication.service';
import {Job} from '../shared/models/job.model';
import {User} from '../shared/models/user.model';
import {forkJoin, Observable, Subscription} from 'rxjs';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.scss']
})
export class FavoriteComponent implements OnInit, OnDestroy {

  public aFavoriteJobs: Job[] = [];
  private localUser: User = new User();

  // Subscriptions
  private aSubscriptions: Subscription[] = [];

  constructor(private jobsService: JobsService, private authenticationService: AuthenticationService) {
  }

  ngOnInit(): void {
    // Get Logged User
    this.localUser = JSON.parse(localStorage.getItem('currentUser') || '{}');

    // Batch request to get details on Liked Jobs
    if (this.localUser && this.localUser.likedJobs && this.localUser.likedJobs.length > 0) {
      const aBatchRequest: Observable<Job>[] = [];
      this.localUser.likedJobs.forEach((skillId: number) => {
        aBatchRequest.push(this.jobsService.getJobById(skillId));
      });
      if (aBatchRequest.length > 0) {
        const batchSubscription = forkJoin(aBatchRequest).subscribe((jobs: Job[]) => {
          jobs.forEach((job: Job) => this.aFavoriteJobs.push(job));
        });
        this.aSubscriptions.push(batchSubscription);
      }
    }
  }

  ngOnDestroy(): void {
    this.aSubscriptions.forEach((subscription: Subscription) => subscription.unsubscribe());
  }

  /**
   * Generate a basic mail for a Job
   * @param job: Job
   * @return string
   */
  generateMailTo(job: Job): string {
    const sMailSubject: string = 'Candidature au poste ' + job.title;
    const sMailBody: string = 'Madame, Monsieur,%0D%0A\n' +
      'Je me permets de vous proposer aujourd’hui ma candidature au poste de ' + job.title + '.%0D%0A%0D%0A\n' +
      'Vous trouverez en pièce jointe mon CV et ma lettre de motivation pour en apprendre plus. N’hésitez-pas à me contacter pour d’éventuelles questions.%0D%0A%0D%0A\n' +
      'En attente de votre réponse, je vous prie d’accepter Madame, Monsieur mes salutations distinguées.';
    return 'mailto:' + job.email + '?subject=' + sMailSubject + '&body=' + sMailBody;
  }

  /**
   * Delete a job from user's favorite
   * @param jobToDeleteId: number
   */
  deleteFromFavorites(jobToDeleteId: number): void {
    if (this.localUser && this.localUser.likedJobs && this.localUser.likedJobs.length > 0) {
      // Delete the favorite from the User
      this.localUser.likedJobs.forEach((jobId: number, index: number) => {
        if (this.localUser.likedJobs && jobId === jobToDeleteId) {
          this.localUser.likedJobs.splice(index, 1);
        }
      });
      const updateAccountSubscription = this.authenticationService.updateAccount(this.localUser).subscribe((user: User) => {
        this.localUser = user;
        localStorage.setItem('currentUser', JSON.stringify(this.localUser));

        // Update List
        this.aFavoriteJobs.forEach((job: Job, index: number) => {
          if (job.id === jobToDeleteId) {
            this.aFavoriteJobs.splice(index, 1);
          }
        });
      });
      this.aSubscriptions.push(updateAccountSubscription);
    }
  }

  /**
   * Track object's ID so it doesn't reload the whole DOM
   * @param index: number
   * @param item: Skill
   * @return item's id or null
   */
  trackByFn(index: number, item: Job): number|null {
    if (!item) return null;
    return item.id;
  }

}
