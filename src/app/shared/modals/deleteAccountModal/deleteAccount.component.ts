import {Component} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-deleteaccount-dialog',
  templateUrl: 'deleteAccount.dialog.html',
})
export class DeleteAccountComponent {

  constructor(public dialogRef: MatDialogRef<DeleteAccountComponent>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  onYesClick(): void {
    this.dialogRef.close('deleteAccount');
  }

}
