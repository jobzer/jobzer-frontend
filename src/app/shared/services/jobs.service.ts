import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Job} from '../models/job.model';
import {environment} from '../../../environments/environment';

@Injectable()
export class JobsService {

  constructor(private http: HttpClient) { }

  /**
   * Returns an Array which contains all the jobs
   * @returns Observable<Job[]>
   */
  public getAllJobs(): Observable<Job[]> {
    return this.http.get<Job[]>(environment.url + `/jobs`);
  }

  /**
   * Returns a specific job
   * @param jobId: number
   * @returns Observable<Job>
   */
  public getJobById(jobId: number): Observable<Job> {
    return this.http.get<Job>(environment.url + `/jobs/${jobId}`);
  }
}
