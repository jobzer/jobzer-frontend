import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {User} from '../models/user.model';
import {Authentication} from '../models/authentication.model';
import {HttpClient} from '@angular/common/http';
import {map, catchError} from 'rxjs/operators';
import {environment} from '../../../environments/environment';

@Injectable()
export class AuthenticationService {

  constructor(private http: HttpClient) {
  }

  /**
   * Returns the logged user from the cache
   * @returns Observable<User>
   */
  public getUserLoggedIn(): Observable<User> {
    return of(JSON.parse(localStorage.getItem('currentUser') || '{}') as User);
  }

  /**
   * Returns the user's first name
   * @returns string
   */
  public getUserFirstName(): string {
    return localStorage.getItem('currentUser') ? (JSON.parse(localStorage.getItem('currentUser') || '{}') as User).informations.firstname : '';
  }

  /**
   * Returns the user's profile picture
   * @returns string
   */
  public getUserProfilePicture(): string {
    return localStorage.getItem('currentUser') ? (JSON.parse(localStorage.getItem('currentUser') || '{}') as User).profilePicture : '';
  }

  /**
   * Try to login the user and returns the result
   * @param email: string
   * @param password: string
   * @returns Observable<Authentification>
   */
  public login(email: string, password: string): Observable<Authentication> {
    if(!environment.production) {
      return this.http.get<User[]>(environment.url + `/users?email=${email}`).pipe(map((user: User[]) => {
        const returnedUser: User = (user[0] as User);

        if (returnedUser && (returnedUser.email === email && returnedUser.password === password)) {
          const loggedUser: Authentication = {
            code: 200,
            user: returnedUser
          };
          localStorage.setItem('currentUser', JSON.stringify(returnedUser));
          return loggedUser;
        } else {
          const errorLogin: Authentication = {
            code: 401,
            error: 'Identifiants incorrects'
          };
          return errorLogin;
        }

      }));
    }
    return this.http.get<User>(environment.url + `/users?email=${email}&password=${btoa(password)}`).pipe(
      map(user => {
        const loggedUser: Authentication = {
          code: 200,
          user: user
        };
        localStorage.setItem('currentUser', JSON.stringify(user));
        return loggedUser;
      }),
      catchError(() => of({
          code: 401,
          error: 'Identifiants incorrects'
        })
      ));
  }

  /**
   * Delete the user from cache (logout)
   */
  public logout(): void {
    localStorage.clear();
  }

  /**
   * Verify if the user exists in cache (if so, he's logged)
   * @returns true | false
   */
  public isUserLoggedIn(): boolean {
    return !localStorage.getItem('currentUser');
  }

  /**
   * Try to register the user and returns the body's reponse
   * @param user: User
   * @returns Object
   */
  public register(user: User): Observable<User> {
    return this.http.post<User>(environment.url + `/users`, user, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map(reponse => (reponse.body as User)));
  }

  /**
   * Try to delete the user and returns the body's reponse
   * @param user: User
   * @returns Object
   */
  public deleteAccount(user: User): Observable<User> {
    return this.http.delete<User>(environment.url + `/users/${user.id}`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map(reponse => (reponse.body as User)));
  }

  /**
   * Try to update the user and returns the body's reponse
   * @param user: User
   * @returns Object
   */
  public updateAccount(user: User): Observable<User> {
    return this.http.put<User>(environment.url + `/users/${user.id}`, user, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map(reponse => (reponse.body as User)));
  }
}
