import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Skill} from '../models/skill.model';
import {environment} from '../../../environments/environment';

@Injectable()
export class SkillsService {

  constructor(private http: HttpClient) { }

  /**
   * Returns an Array which contains all the skills
   * @returns Observable<Skill[]>
   */
  public getAllSkills(): Observable<Skill[]> {
    return this.http.get<Skill[]>(environment.url + `/skills`);
  }
}
