export class Error {
  code?: number;
  error: boolean = false;
  message: string = '';
}
