import {Skill} from './skill.model';

export class Job {
  id: number = 0;
  company: string = '';
  email: string = '';
  phone: string = '';
  title: string = '';
  detail: string = '';
  skills: Skill[] = [];
}
