import {User} from './user.model';

export class Authentication {
  code: number = 0;
  error?: string;
  user?: User;
}
