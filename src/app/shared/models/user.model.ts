import {Skill} from './skill.model';

export class User {
  id?: number;
  password: string = '';
  email: string = '';
  emailVerified: boolean = false;
  profilePicture: string = '';
  likedJobs?: number[];
  dislikedJobs?: number[];
  informations: Informations = new Informations();
  skills?: Skill[];
}

export class Informations {
  firstname: string = '';
  lastname: string = '';
  birthdate: string = '';
}
