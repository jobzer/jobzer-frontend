import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router) { }

  /**
   * Verify if the user is logged in so he can access or not some pages
   * @returns true | false
   */
  canActivate() {
    if (localStorage.getItem('currentUser')) {
      return true;
    }

    this.router.navigate(['home']).then();
    return false;
  }
}
