import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {LoginComponent} from './login.component';
import {AuthenticationService} from '../shared/services/authentication.service';
import {User} from '../shared/models/user.model';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {FaIconComponent, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {Authentication} from '../shared/models/authentication.model';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {TranslateModule} from '@ngx-translate/core';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authenticationService: AuthenticationService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [BrowserModule, FontAwesomeModule, ReactiveFormsModule, FormsModule, HttpClientModule, RouterTestingModule, TranslateModule.forRoot()],
      declarations: [LoginComponent, FaIconComponent],
      providers: [AuthenticationService]
    })
      .compileComponents();

    // Mock localStorage
    let store: object = {};
    spyOn(window.localStorage, 'getItem').and.callFake((key: string) => {
      return key in store ? JSON.parse(((store as any)[key] as string) || '{}') : '{}';
    });
    spyOn(window.localStorage, 'setItem').and.callFake((key: string, value: string) => {
      return (store as any)[key] = JSON.stringify(value);
    });
    spyOn(window.localStorage, 'clear').and.callFake(() => {
      store = {};
    });
    spyOn(window.localStorage, 'removeItem').and.callFake((key: string) => {
      delete (store as any)[key];
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    authenticationService = TestBed.inject(AuthenticationService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('authentication service should create', () => {
    expect(authenticationService).toBeTruthy();
  });

  it('Should pass login', (done) => {
    authenticationService.login('joe@moma.com', 'plain-text-password').subscribe((login: Authentication) => {
      expect(login.code === 200).toBeTruthy();
      done();
    });
  });

  it('Should get user email', (done) => {
    authenticationService.login('joe@moma.com', 'plain-text-password').subscribe((login: Authentication) => {
      if (login.code === 200) {
        authenticationService.getUserLoggedIn().subscribe((user: User) => {
          expect(user.email).toEqual('joe@moma.com');
          done();
        });
      } else {
        fail(login.error);
        done();
      }
    });
  });

  it('Should get user id', (done) => {
    authenticationService.login('joe@moma.com', 'plain-text-password').subscribe((login: Authentication) => {
      if (login.code === 200) {
        authenticationService.getUserLoggedIn().subscribe((user: User) => {
          expect(user.id).toEqual(1);
          done();
        });
      } else {
        fail(login.error);
        done();
      }
    });
  });

  it('Should not pass login (incorrect credentials)', (done) => {
    authenticationService.login('joe@moma.com', 'oupsie').subscribe((login: Authentication) => {
      expect(login.code === 401).toBeTruthy();
      done();
    });
  });

});
