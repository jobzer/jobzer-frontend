import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthenticationService} from '../shared/services/authentication.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Authentication} from '../shared/models/authentication.model';
import {Error} from '../shared/models/error.model';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  // Form
  public loginForm: FormGroup;
  public hasLogginFailed: Error = {error: false, message: ''};

  // Subscriptions
  private aSubscriptions: Subscription[] = [];

  constructor(private authenticationService: AuthenticationService, private formBuilder: FormBuilder, private router: Router, private translate: TranslateService) {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    }, {
      updateOn: 'change'
    });
  }

  ngOnInit(): void {
    this.hasLogginFailed = {error: false, message: ''};

    // If user's already logged-in, redirect to the homepage
    if (environment.production && localStorage.getItem('currentUser')) {
      this.router.navigate(['home']).then();
    }
  }

  ngOnDestroy(): void {
    this.aSubscriptions.forEach((subscription: Subscription) => subscription.unsubscribe());
  }

  /**
   * Try to login the user
   */
  onSubmit(): void {
    const userLogin = this.loginForm.get('email');
    const userPassword = this.loginForm.get('password');
    if (userLogin && userPassword) {
      const loginSubscription = this.authenticationService.login(userLogin.value, userPassword.value).subscribe((login: Authentication) => {
        if (login.code === 200) {
          if (login.user && !login.user.emailVerified) {
            this.hasLogginFailed = {
              error: true,
              message: this.translate.instant('login.wrongCredentials', {email: (login.user ? login.user.email : 'Error loginUser')})
            };
          } else {
            this.hasLogginFailed = {error: false, message: ''};
            this.router.navigate(['profile']).then();
          }
        } else if (login.code === 401) {
          this.hasLogginFailed = {error: true, message: this.translate.instant('login.wrongCredentials')};
        }
      });
      this.aSubscriptions.push(loginSubscription);
    }
  }
}
